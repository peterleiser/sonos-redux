require 'httparty'

module SonosRedux::Device

  class Base
    require 'sonos_redux/device/upnp'
    require 'sonos_redux/service/base'

    include SonosRedux::Device::Upnp

    attr_reader :ssdp_search_result
    attr_reader :description
    attr_reader :services # see add_services() below
    attr_reader :service_names

    def initialize(ssdp_search_result)
      puts "SonosRedux::Device::Base.init"

      hash = ssdp_search_result
      @ssdp_search_result = OpenStruct.new ssdp_search_result
      @ssdp_search_result.params = OpenStruct.new @ssdp_search_result.params

      setup_upnp_device
      add_services
    end

    # returns contents of "LOCATION"=>"http://<ip>:1400/xml/device_description.xml"
    def description
      unless @description
        # @description = HTTParty.get(ssdp_search_result.params.LOCATION).parsed_response
        
        body = HTTParty.get(ssdp_search_result.params.LOCATION).body
        # XML for Sonos Connect (Sonos 1) includes three lines that start with "#DEACTIVATION"
        # This is incorrect XML syntax, so we strip it out
        sanitized_body = body.split("\n").grep_v(/#DEACTIVATION/).join("\n")
        @description = MultiXml.parse(sanitized_body)
      end

      @description
    end

    protected

    def create_method(name, &block)
      self.class.send(:define_method, name, &block)
    end

    # Documentation on EasyUpnp: https://www.rubydoc.info/gems/easy_upnp
    #
    # Use EasyUpnp to SOAP services from all of the serviceLists, which will then
    # allow the device to use those service's remote methods.  For example, a
    # service from http://<ip>:1400/xml/device_description.xml.xml that is:
    # <serviceType>urn:schemas-upnp-org:service:SystemProperties:1</serviceType>
    # where the remote methods are defined in:
    # http://<ip>:1400/xml/SystemProperties1.xml
    # will be callable via device.system_properties.<action name>,
    # e.g. device.system_properties.GetWebCode()
    #
    # A Sonos Play:1 will typically have these services after calling this method:
    # content_directory
    # connection_manager
    # rendering_control
    # av_transport
    # queue
    # group_rendering_control
    # virtual_line_in
    # alarm_clock
    # music_services
    # device_properties
    # system_properties
    # zone_group_topology
    # group_management
    # q_play
    def add_services
      @services = {}
      @service_names = []
      upnp_device.all_services.each do |service_urn|
        service_name = service_urn.split(":")[-2].snakecase
        @service_names << service_name

        create_method(service_name.to_sym) {
          unless @services[service_name]
            # upnp_device.service() returns a service instance that exposes
            # the SOAP methods and allows you to call those remote methods
            @services[service_name] = upnp_device.service(service_urn)
          end

          @services[service_name]
        }
      end

    end

  end
end
