require 'sonos_redux/device/base'
require 'sonos_redux/device/upnp'
require 'sonos_redux/queue'

module SonosRedux::Device

  class Device < SonosRedux::Device::Base
    METHODS_INSTANCE_ID_ONLY = [:RemoveAllTracksFromQueue,
                                :BackupQueue,
                                :GetMediaInfo,
                                :GetTransportInfo,
                                :GetPositionInfo,
                                :GetDeviceCapabilities,
                                :GetTransportSettings,
                                :GetCrossfadeMode,
                                :Stop,
                                :Pause,
                                :Next,
                                :Previous,
                                :GetCurrentTransportActions,
                                :BecomeCoordinatorOfStandaloneGroup,
                                :GetRemainingSleepTimerDuration,
                                :GetRunningAlarmProperties,
                                :EndDirectControlSession]

    METHODS_INSTANCE_ID_ONLY.each do |m|
      # 1990's Java want's it redundant "getters" back :-)
      device_method = m.to_s.snakecase.gsub("get_", "").to_sym
      define_method(device_method) { av_transport.send(m, { InstanceID: 0 }) }
    end

    attr_reader :current_queue
    attr_reader :curated_services

    def initialize(ssdp_search_result)
      super
      @current_queue = SonosRedux::Queue::Queue.new(self)
      @curated_services = {}
      # register the curated service: music_services
      unless self.music_services.nil?
        @curated_services['music_services'] = SonosRedux::Service::MusicServices.new(self.music_services)
      end
    end

    def room_name
      keys = %w{ root device }
      description.dig(*keys)['roomName']
    end

    def ip_address
      ssdp_search_result.address
    end

    def play
      av_transport.Play(InstanceID: 0, Speed: 1)
    end

    def watch
      while true
        pi = position_info
        position = "%s / %s" % [ pi[:RelTime], pi[:TrackDuration] ]
        puts position
        sleep 1
      end
    end

  end

end
  

