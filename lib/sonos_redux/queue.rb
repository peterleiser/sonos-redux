module SonosRedux::Queue

  class Queue
    ITEM_TYPES = {
      "album": {
        "prefix": "x-rincon-cpcontainer:1004206c",
        "id": "00040000",
        "class": "object.container.album.musicAlbum",
      },
      "track": {
        "prefix": "",
        "id": "00032020",
        "class": "object.item.audioItem.musicTrack",
      },
      "playlist": {
        "prefix": "x-rincon-cpcontainer:1006206c",
        "id": "1006206c",
        "class": "object.container.playlistContainer",
      },
    }

    MUSIC_SERVICE_NUMBERS = {
      Tidal: 44551
    }

    attr_reader :device

    def initialize(device)
      @device = device
    end

    def list
      input_args = {
        ObjectID: "Q:0",
        BrowseFlag: "BrowseDirectChildren",
        Filter: "*",
        StartingIndex: 0,
        RequestedCount: 100,
        SortCriteria: ""
      }
      result = device.content_directory.Browse(input_args)
      keys = %w{ DIDL_Lite item }
      MultiXml.parse(result[:Result]).dig(* keys)
    end

    def add_from_tidal(uri)
      item = item(uri)
      track_uri = "#{item[:type]}%2f#{item[:encoded_uri]}"
      enqueue_uri = ITEM_TYPES[item[:type]][:prefix] + track_uri
      item_id = ITEM_TYPES[item[:type]][:id] + track_uri
      item_class = ITEM_TYPES[item[:type]][:class]
      sn = MUSIC_SERVICE_NUMBERS[:Tidal]

      metadata=<<BLOCK
      <DIDL-Lite
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/"
        xmlns:r="urn:schemas-rinconnetworks-com:metadata-1-0/"
        xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/">
        <item id="#{item_id}" restricted="true">
          <upnp:class>#{item_class}</upnp:class>
          <desc id="cdudn" nameSpace="urn:schemas-rinconnetworks-com:metadata-1-0/">SA_RINCON#{sn}_X_#Svc#{sn}-0-Token</desc>
        </item>
      </DIDL-Lite>
BLOCK

      position = 0 # Place tracks starting at beginning of queue
      as_next = 0 # False

      input_args = {
        InstanceID: 0,
        EnqueuedURI: enqueue_uri,
        EnqueuedURIMetaData: sanitize_xml(metadata),
        DesiredFirstTrackNumberEnqueued: position,
        EnqueueAsNext: as_next
      }

      result = {
        input_args: input_args,
        metadata: metadata
      }

      begin
        result[:response] = device.av_transport.AddURIToQueue(input_args)
      rescue => e
        result[:error] = e
      end

      result

      # keys = %w{ DIDL_Lite item }
      # MultiXml.parse(result[:Result]).dig(* keys)

    end

    # private

    def item(uri)
      m = /(?<type>album|track|playlist)\/(?<encoded_uri>[\w]+)/.match(uri)
      {
        type: m['type'].to_sym,
        encoded_uri: m['encoded_uri']
      }
    end

    def sanitize_xml(xml_string)
        xml_string.gsub("\n", "").gsub("\t", "").gsub(/[[:space:]]+/, ' ').gsub(/>[[:space:]]+/, '>').gsub(/[[:space:]]+</, '<')
    end


  end

end


