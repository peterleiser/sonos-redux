require 'ssdp'
require 'sonos_redux/device'

module SonosRedux::Discovery
  attr_reader :devices

  # This can be called to both initially discover and rediscover devices
  def discover_devices
    discovery = Discovery.new
    discovery.discover_devices
    @devices = discovery.ssdp_search_results.map do |ssdp_search_result|
      SonosRedux::Device::Device.new(ssdp_search_result)
    end
  end

  class Discovery
    attr_reader :ssdp_search_results

    MULTICAST_PORT = 1900
    DEFAULT_TIMEOUT = 3
    DISCOVERY_URN = 'urn:schemas-upnp-org:device:ZonePlayer:1'

    attr_reader :timeout

    def initialize(timeout = DEFAULT_TIMEOUT)
      @timeout = timeout
    end

    def discover_devices
      search_params = {
        service: DISCOVERY_URN,
        first_only: false,
        timeout: timeout,
        filter: lambda {|r| r[:params]["ST"].match(/ZonePlayer/) }
      }

      ssdp_options = {
        broadcast: '255.255.255.255',
        # broadcast: '239.255.255.250',
        # port: 1900
      }
      @ssdp_search_results = SSDP::Consumer.new(ssdp_options).search(search_params)
    end

  end

end
