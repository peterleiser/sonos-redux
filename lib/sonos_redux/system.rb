
module SonosRedux

  class System
    require 'sonos_redux/discovery'
    require 'sonos_redux/zone_group'

    include SonosRedux::Discovery
    include SonosRedux::ZoneGroup

    def initialize(discover=true)
      if discover
        discover_devices
        zone_groups_for_devices(devices)
      else
        @devices = []
      end
    end

    def add_device(ip_address)
      # Use the existing machinery we built for ssdp_search
      synthetic_ssdp_search_result = {
        address: ip_address,
        params: {
          LOCATION: "http://#{ip_address}:1400/xml/device_description.xml"
        }
      }
      devices << SonosRedux::Device::Device.new(synthetic_ssdp_search_result)
      zone_groups_for_devices(devices)
    end

    def info
      devices.map do |device|
        data = device.description['root']['device']
        [
          data['friendlyName'],
          data['roomName']
        ]
      end.sort_by{|i| i.last}
    end

  end

end
