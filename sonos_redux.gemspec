# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sonos_redux/version'

Gem::Specification.new do |gem|
  gem.name          = 'sonos_redux'
  gem.version       = SonosRedux::VERSION
  gem.authors       = ['Peter Leiser']
  gem.email         = ['sonos-redux@peterleiser.com']
  gem.description   = 'Control Sonos speakers in 2020 with Ruby'
  gem.summary       = gem.description
  gem.homepage      = 'https://github.com/peterleiser/sonos-redux'
  gem.license       = 'MIT'

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.required_ruby_version = '>= 2.7'
  gem.add_dependency 'ssdp'
  gem.add_dependency 'easy_upnp' # https://www.rubydoc.info/gems/easy_upnp/
end
