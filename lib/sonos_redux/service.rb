require 'sonos_redux/service/base'

module SonosRedux::Service

  class Service < SonosRedux::Service::Base
  end

  class MusicServices < Service
    attr_reader :types
    attr_reader :version
    attr_reader :name_to_descriptor

    def initialize(service)
      super
      available = @service.ListAvailableServices
      @types = available[:AvailableServiceTypeList].split(",").map(&:to_i)
      @version = available[:AvailableServiceListVersion]

      @name_to_descriptor = {}
      descriptors = MultiXml.parse(available[:AvailableServiceDescriptorList])['Services']['Service']
      descriptors.each{|d| @name_to_descriptor[d['Name']] = d}
    end

    def names
      @name_to_descriptor.keys.sort
    end

    def descriptors
      @name_to_descriptor.values
    end

    def descriptor(name)
      @name_to_descriptor[name]
    end

  end

end
