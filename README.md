# SonosRedux

To get started:

`bundle install`

This loads the gem and stars irb:

```rake console```

Then, within irb you can do the following:
* Discover devices
* View system info
* Get ZoneGroup names (Stereo pairs, rooms, groups)
* View contents of http://<ip>:1400/xml/device_description.xml
* Get the coordinator device for a ZoneGroup
* View what remote SOAP services are available on a device
* View what remote SOAP methods are available for a service
* View what the arguments are for a remote SOAP method
* Call a method, like Stop and Play, on a remote SOAP service

```
# Discover your devices
system = SonosRedux::System.new

# Print out info about the devices
# IP address / Product Name / Room Name
pp system.info

[["192.168.1.119 - Sonos Play:1", "Dining Room"],
 ["192.168.1.203 - Sonos Play:1", "Dining Room"],
 ["192.168.1.191 - Sonos Play:1", "Kitchen"],
 ["192.168.1.218 - Sonos Play:1", "Kitchen"],
 ["192.168.1.184 - Sonos Play:1", "Living Room"],
 ["192.168.1.212 - Sonos Play:1", "Living Room"],
 ["192.168.1.193 - Sonos Play:1", "Outside"],
 ["192.168.1.219 - Sonos Play:1", "Outside"],
 ["192.168.1.116 - Sonos Play:1", "Playroom"],
 ["192.168.1.231 - Sonos Play:1", "Playroom"]]

# Names of your zone groups
pp system.zone_group_names

Dining Room
Kitchen
Living Room
Outside
Playroom

# Take the first device, and then print out the contents from
# http://<ip>:1400/xml/device_description.xml
# that were retrieved and stored in the Device object during discovery
pp system.devices.first.description

{"root"=>
  {"specVersion"=>{"major"=>"1", "minor"=>"0"},
   "device"=>
    {"deviceType"=>"urn:schemas-upnp-org:device:ZonePlayer:1",
     "friendlyName"=>"192.168.1.193 - Sonos Play:1",
     "manufacturer"=>"Sonos, Inc.",
     "manufacturerURL"=>"http://www.sonos.com",
     "modelNumber"=>"S12",
     "modelDescription"=>"Sonos Play:1",
     "modelName"=>"Sonos Play:1",
     "modelURL"=>"http://www.sonos.com/products/zoneplayers/S12",

     ...

# Get the coordinator device for a ZoneGroup.  The coordinator is
# the main device for the stereo pair or group.  Actions performed
# on the coordinator (e.g. Play, Stop) will be done for the pair/group.
playroom = system.coordinator_device('Playroom')

# See what services the coordinator device has
puts playroom.service_names.sort

alarm_clock
av_transport
connection_manager
content_directory
device_properties
group_management
group_rendering_control
music_services
q_play
queue
rendering_control
system_properties
virtual_line_in
zone_group_topology

# Stop and then Play.  First, see what a service error looks like:
playroom.av_transport.Stop

Savon::SOAPFault ((s:Client) UPnPError)

# OK, what are the service methods?
playroom.av_transport.service_methods.sort

AddMultipleURIsToQueue
AddURIToQueue
AddURIToSavedQueue
BackupQueue
BecomeCoordinatorOfStandaloneGroup
BecomeGroupCoordinator
BecomeGroupCoordinatorAndSource
ChangeCoordinator
ChangeTransportSettings
ConfigureSleepTimer
CreateSavedQueue
DelegateGroupCoordinationTo
EndDirectControlSession
GetCrossfadeMode
GetCurrentTransportActions
GetDeviceCapabilities
GetMediaInfo
GetPositionInfo
GetRemainingSleepTimerDuration
GetRunningAlarmProperties
GetTransportInfo
GetTransportSettings
Next
NotifyDeletedURI
Pause
Play
Previous
RemoveAllTracksFromQueue
RemoveTrackFromQueue
RemoveTrackRangeFromQueue
ReorderTracksInQueue
ReorderTracksInSavedQueue
RunAlarm
SaveQueue
Seek
SetAVTransportURI
SetCrossfadeMode
SetNextAVTransportURI
SetPlayMode
SnoozeAlarm
StartAutoplay
Stop

# Now, how do I use Stop?
pp playroom.av_transport.service_method(:Stop);nil

#<EasyUpnp::ServiceMethod:0x00007f997e088760
@arg_references={:InstanceID=>:A_ARG_TYPE_InstanceID},
@in_args=[:InstanceID],
@name=:Stop,
@out_args=[]>

# Stop needs an :InstanceID argument (the @in_args).  Use 0:
playroom.av_transport.Stop(InstanceID: 0)

 => {}

# Now, how do I use Play?
pp playroom.av_transport.service_method(:Play);nil

#<EasyUpnp::ServiceMethod:0x00007f997e096ae0
 @arg_references=
  {:InstanceID=>:A_ARG_TYPE_InstanceID, :Speed=>:TransportPlaySpeed},
 @in_args=[:InstanceID, :Speed],
 @name=:Play,
 @out_args=[]>

# Play needs an :InstanceID and :Speed argument (the @in_args).  
# Use 0 again for :InstanceID and normal speed is :Speed => 1
playroom.av_transport.Play(InstanceID: 0, Speed: 1)

=> {}

# Get the current track
position_info = playroom.av_transport.GetPositionInfo(InstanceID: 0)
position_info[:TrackMetaData] = Nokogiri::XML(position_info[:TrackMetaData]).to_xml

3.0.0 :196 > pp position_info;nil
{:Track=>"19",
 :TrackDuration=>"0:03:22",
 :TrackMetaData=>
  "<?xml version=\"1.0\"?>\n" +
  "<DIDL-Lite xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" xmlns:r=\"urn:schemas-rinconnetworks-com:metadata-1-0/\" xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\">\n" +
  "  <item id=\"-1\" parentID=\"-1\" restricted=\"true\">\n" +
  "    <res protocolInfo=\"sonos.com-http:*:audio/flac:*\" duration=\"0:03:22\">x-sonos-http:track%2f189591381.flac?sid=174&amp;flags=24616&amp;sn=1</res>\n" +
  "    <r:streamContent/>\n" +
  "    <upnp:albumArtURI>/getaa?s=1&amp;u=x-sonos-http%3atrack%252f189591381.flac%3fsid%3d174%26flags%3d24616%26sn%3d1</upnp:albumArtURI>\n" +
  "    <dc:title>Sexting</dc:title>\n" +
  "    <upnp:class>object.item.audioItem.musicTrack</upnp:class>\n" +
  "    <dc:creator>Bo Burnham</dc:creator>\n" +
  "    <upnp:album>INSIDE</upnp:album>\n" +
  "    <r:tags>1</r:tags>\n" +
  "  </item>\n" +
  "</DIDL-Lite>\n",
 :TrackURI=>"x-sonos-http:track%2f189591381.flac?sid=174&flags=24616&sn=1",
 :RelTime=>"0:01:28",
 :AbsTime=>"NOT_IMPLEMENTED",
 :RelCount=>"2147483647",
 :AbsCount=>"2147483647"}
 => nil 
    
```
