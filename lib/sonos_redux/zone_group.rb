module SonosRedux::ZoneGroup
  attr_reader :zone_groups

  def zone_groups_for_devices(devices)
    @zone_groups = ZoneGroup.zone_groups(devices)
  end

  def zone_group_names
    zone_groups.map(&:current_zone_group_name).compact.sort
  end

  def zone_group_uuid(zone_group_name)
    zone_groups.find{|zg| zg.current_zone_group_name == zone_group_name}.current_zone_player_uuids_in_group
  end

  def coordinator_device(zone_group_name)
    uuid = zone_group_uuid(zone_group_name)
    devices.find{|d| d.description.dig('root', 'device', 'UDN').include? uuid}
  end

  def coordinator_devices
    devices.select{|d| not d.zone_group_topology.GetZoneGroupAttributes[:CurrentZoneGroupID].nil?}
  end

  class ZoneGroup
    require 'facets'

    # The attr_accessor's are done this way to convey to the developer
    # that these are the underlying properties from the XML that are used.
    # It also serves to reduce the the amount of code for readabilty and is
    # more of a data driven process.
    attr_accessor :CurrentZoneGroupName.to_s.snakecase.to_sym
    attr_accessor :CurrentZoneGroupID.to_s.snakecase.to_sym
    attr_accessor :CurrentZonePlayerUUIDsInGroup.to_s.gsub('UUID', 'Uuid').snakecase
    attr_accessor :CurrentMuseHouseholdId.to_s.snakecase.to_sym

    attr_reader :zone_groups

    def initialize(params)
      params.each do |name, value|
        self.send("#{name.to_s.gsub('UUID', 'Uuid').snakecase}=", value)
      end
    end

    def self.zone_groups(devices)
      devices.map do |device|
        params = device.zone_group_topology.GetZoneGroupAttributes
        ZoneGroup.new(params)
      end
    end

  end

end
