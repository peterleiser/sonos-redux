require 'easy_upnp'

# monkey patch for easy_upnp that calls deprecated Kernel.open instead of URI.open
require 'forwardable'

module EasyUpnp
  class DeviceControlPoint
    require 'forwardable'
    self.extend Forwardable
    def_delegator "URI", "open", "open"
  end
end

module EasyUpnp
  class DeviceControlPoint

    def self.from_service_definition(definition, options, &block)
      urn = definition[:st]
      root_uri = definition[:location]

      xml = Nokogiri::XML(URI.open(root_uri))
      xml.remove_namespaces!

      service = xml.xpath("//device/serviceList/service[serviceType=\"#{urn}\"]").first

      if service.nil?
        raise RuntimeError, "Couldn't find service with urn: #{urn}"
      else
        service = Nokogiri::XML(service.to_xml)
        service_definition_uri = URI.join(root_uri, service.xpath('service/SCPDURL').text).to_s
        service_definition = URI.open(service_definition_uri) { |f| f.read }

        endpoint_url = ->(xpath) do
          URI.join(root_uri, service.xpath(xpath).text).to_s
        end

        DeviceControlPoint.new(
            urn,
            endpoint_url.call('service/controlURL'),
            endpoint_url.call('service/eventSubURL'),
            service_definition,
            options,
            &block
        )
      end
    end

  end
end

module SonosRedux::Device::Upnp
  attr_reader :upnp_device

  protected

  def setup_upnp_device
    uuid = description['root']['device']['UDN']
    service_definitions = {}

    location = ssdp_search_result.params.LOCATION

    # we need to combine service lists from uuid, uuid_MS, and uuid_MR
    combined_service_lists = []

    # _MS & _MR
    description['root']['device']['deviceList']['device'].map do |device|
      combined_service_lists += device['serviceList']['service'] unless device.class == Array
    end

    # top level uuid
    combined_service_lists += description['root']['device']['serviceList']['service']

    # remove duplicates
    combined_service_lists.uniq!{|s| s['serviceType']}

    service_definitions = combined_service_lists.map do |service|
      {
        location: location,
        st: service['serviceType']
      }
    end

    @upnp_device = EasyUpnp::UpnpDevice.new(uuid, service_definitions)
  end

end
